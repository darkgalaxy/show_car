﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;
using UnityEngine.UI;

public class MouseRotation : MonoBehaviour, IPointerDownHandler, IPointerUpHandler
{
    float RotationSpeedOfMouse = 500;
    public GameObject carro;

    private bool pointerDown;
    
    public void OnPointerDown(PointerEventData eventData)
    {
        pointerDown = true;
    }

    public void OnPointerUp(PointerEventData eventData)
    {
        Reset();
    }
    public void Reset()
    {
        pointerDown = false;
    }
    // Start is called before the first frame update
    void Start()
    {
    }
    // Update is called once per frame
    void Update()
    {   
        if(pointerDown)
        {
            if (this.gameObject.name == "Seta_Esquerda")
            {
                float x = RotationSpeedOfMouse * Mathf.Deg2Rad;
                carro.transform.Rotate(Vector3.up, x);
            }
            if (this.gameObject.name == "Seta_Direita")
            {
                float x = RotationSpeedOfMouse * Mathf.Deg2Rad;
                carro.transform.Rotate(Vector3.down, x);
            }
        }
    }
}
