﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeMaterial : MonoBehaviour
{
    [SerializeField]
    private Material material;

    [SerializeField]
    private GameObject carro;
    [SerializeField]
    private int indiceCarro;

    [SerializeField]
    private GameObject portaEsquerda;
    [SerializeField]
    private int IndicePortaEsquerda;

    [SerializeField]
    private GameObject portaDireita;
    [SerializeField]
    private int IndicePortaDireita;

    [SerializeField]
    private GameObject meio;
    [SerializeField]
    private int IndiceMeio;

    [SerializeField]
    private GameObject tras;
    [SerializeField]
    private int IndiceTras;

    Material[] matsCarro;
    Material[] matsPortaEsquerda;
    Material[] matsPortaDireita;
    Material[] matsMeio;
    Material[] matsTras;

    public void MaterialChanging()
    {
        matsCarro = carro.GetComponent<Renderer>().materials;
        matsCarro[indiceCarro] = material;
        carro.GetComponent<Renderer>().materials = matsCarro;

        matsPortaEsquerda = portaEsquerda.GetComponent<Renderer>().materials;
        matsPortaEsquerda[IndicePortaEsquerda] = material;
        portaEsquerda.GetComponent<Renderer>().materials = matsPortaEsquerda;

        matsPortaDireita = portaDireita.GetComponent<Renderer>().materials;
        matsPortaDireita[IndicePortaDireita] = material;
        portaDireita.GetComponent<Renderer>().materials = matsPortaDireita;
    }

    public void MaterialChanging1Porta()
    {
        matsCarro = carro.GetComponent<Renderer>().materials;
        matsCarro[indiceCarro] = material;
        carro.GetComponent<Renderer>().materials = matsCarro;

        matsPortaEsquerda = portaEsquerda.GetComponent<Renderer>().materials;
        matsPortaEsquerda[IndicePortaEsquerda] = material;
        portaEsquerda.GetComponent<Renderer>().materials = matsPortaEsquerda;
    }

    public void MaterialChanging2Porta()
    {
        matsCarro = carro.GetComponent<Renderer>().materials;
        matsCarro[indiceCarro] = material;
        carro.GetComponent<Renderer>().materials = matsCarro;

        matsPortaEsquerda = portaEsquerda.GetComponent<Renderer>().materials;
        matsPortaEsquerda[IndicePortaEsquerda] = material;
        portaEsquerda.GetComponent<Renderer>().materials = matsPortaEsquerda;

        matsPortaDireita = portaDireita.GetComponent<Renderer>().materials;
        matsPortaDireita[IndicePortaDireita] = material;
        portaDireita.GetComponent<Renderer>().materials = matsPortaDireita;

        matsMeio = meio.GetComponent<Renderer>().materials;
        matsMeio[IndiceMeio] = material;
        meio.GetComponent<Renderer>().materials = matsMeio;

        matsTras = tras.GetComponent<Renderer>().materials;
        matsTras[IndiceTras] = material;
        tras.GetComponent<Renderer>().materials = matsTras;
    }
}
