﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ChangeSprite : MonoBehaviour
{
    [SerializeField]
    private Sprite[] imagens;

    [SerializeField]
    private Image imagemFonte;

    [SerializeField]
    private GameObject esquerda;
    [SerializeField]
    private GameObject direita;

    private int indice;
    private int cont = 0;

    private void Awake()
    {
        indice = imagens.Length;
    }

    public void TrocarImagem(GameObject sendoUsado)
    {
        if (sendoUsado.name == esquerda.name)
        {
            --cont;
            if (cont < 0)
            {
                cont = 0;
            }
            if (cont > 0 && cont < indice)
            { 
                imagemFonte.sprite = imagens[cont]; 
            }
        }
        if(sendoUsado.name== direita.name)
        {
            ++cont;
            if (cont > indice)
            {
                cont = indice;
            }
            if (cont < indice && cont>=0)
            {
                imagemFonte.sprite = imagens[cont];
                ++cont;
            }
        }

    }
}
